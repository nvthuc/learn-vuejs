'use strict';


var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello from Vue !'
    }
})

var app2 = new Vue({
    el: '#app2',
    data: {
        message: "Now is " + new Date().toLocaleDateString()
    }
})

var app3 = new Vue({
    el: '#app3',
    data: {
        seen: false

    }
})

var app4 = new Vue({
    el: '#app4',
    data: {
        todos: [{
                text: "Todo 1"
            },
            {
                text: "Todo 2"
            },
            {
                text: "Todo 3"
            },
            {
                text: "Todo 4"
            },
            {
                text: "Todo 5"
            },
            {
                text: "Todo 6"
            }
        ]
    }
})

var app5 = new Vue({
    el: "#app5",
    data: {
        message: "This is string !"
    },
    methods: {
        onClickReveseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    },
})

var app6 = new Vue({
    el: "#app6",
    data: {
        message: "Change this message to see result"
    }
})

// Define Vue Component
Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{todo.text}}</li>'
})

var app7 = new Vue({
    el: '#app7',
    data: {
        todos: [{
                id: 1,
                text: 'dfdsfdjfklj ldjfld'
            },
            {
                id: 2,
                text: 'dfdsfdjfklj ldjfld'
            },
            {
                id: 5,
                text: 'dfdsfdjfklj ldjfld'
            },
            {
                id: 9,
                text: 'dfdsfdjfklj ldjfld'
            },
        ]
    }
})